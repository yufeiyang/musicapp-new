var express = require("express")
var path = require("path")
// const http = require('http')
// const server = http.createServer(app)

var logger = require("morgan")
var cookieParser = require("cookie-parser")
const bodyParser = require('body-parser')
var routes = require("./server/routes/index")
var music = require("./server/routes/music")
var user = require("./server/routes/user")
var leadboard = require("./server/routes/leadboard")
var cors = require('cors')



var app = express()
var allowCrossDomain = (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')

  next()
}
// view engine setup
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")
app.use(cors())
app.use(logger("dev"))
// app.use(express.json())
// app.use(express.urlencoded({ extended: false }))
//app.use(bodyParser.json({ limit: '10mb' }))
app.use(bodyParser.urlencoded({  extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, "public")))

app.use("/", allowCrossDomain, routes)

app.get("/music",music.showAll)
app.get("/music/:name",music.findOne)
app.post("/music",music.addMusic)
app.delete("/music/:name",music.deleteMusic)
app.put("/music/:id",music.updateMusicInfo)
app.get("/music/:album/album",music.searchAlbum)

app.get("/user/:username",user.searchUser)
app.post("/user",user.addUser)
app.delete("/user/:username",user.deleteUser)
app.put("/user/:username/password",user.updateUserPassword)

app.get("/leadboard",leadboard.showAllBoards)
app.get("/leadboard/:name",leadboard.findBoards)
app.post("/leadboard",leadboard.addBoard)
app.delete("/leadboard/:name",leadboard.deleteBoard)
app.put("/leadboard/:id",leadboard.updateBoard)



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found")
  err.status = 404
  next(err)
})

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get("env") === "development" ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render("error")
})
// server.listen(3002)
app.listen(3001)
module.exports = app
