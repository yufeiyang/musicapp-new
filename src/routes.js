import Login from './components/Login.vue'
import Home from './components/Home.vue'
import Main from './components/Main.vue'
import Music from './components/Music.vue'
import Leadboard from './components/Leadboard.vue'
import Setting from './components/Setting.vue'

let routes = [
  {
    path: '/login',
    component: Login,
    name: '',
    hidden: true
  },
  {
    path: '/',
    component: Home,
    name: 'Find Music',
    iconCls: 'el-icon-message',
    children: [
      { path: '/music', component: Music, name: 'Music' },
    ]
  },
  {
    path: '/',
    component: Home,
    name: 'Leadboard',
    iconCls: 'fa fa-id-card-o',
    children: [
      { path: '/Leadboard', component: Leadboard, name: 'Leadboard' },
    ]
  },
  {
    path: '/',
    component: Home,
    name: 'User',
    children: [
      { path: '/setting', component: Setting, name: 'Change Password' },
    ]
  },
]

export default routes